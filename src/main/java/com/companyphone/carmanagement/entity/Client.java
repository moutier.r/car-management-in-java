package com.companyphone.carmanagement.entity;


public class Client {
	
	
	private Integer id;
	
	
	private String client_name;
	
	
	
	private Boolean active;

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getClientName() {
		return client_name;
	}

	public void setClientName(String clientName) {
		this.client_name = clientName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	

}
