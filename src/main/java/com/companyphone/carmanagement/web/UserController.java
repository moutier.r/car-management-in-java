package com.companyphone.carmanagement.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import rest.RestResponse;
import rest.Result;

@Controller
public class UserController {
	
	
	String instance = "https://dev90361.service-now.com";
	MultiValueMap<String, String> bodyMap = new LinkedMultiValueMap<>();
	HttpHeaders headers = new HttpHeaders();

	// Constructor
	public UserController() {

		this.headers.add("Content-Type", "application/x-www-form-urlencoded");

		this.bodyMap.add("grant_type", "password");
		this.bodyMap.add("client_id", "24f8ac0193c90010eeaf2a0b9529778b");
		this.bodyMap.add("client_secret", "A*.wVB-hhs");
	}

	@GetMapping("/users")
	public String doLogin(HttpServletRequest request, 
			RestTemplate restTemplate, 
			Model model,
			@RequestParam(required = false, defaultValue = "0") Integer page) {
		
		String url = instance + "/api/now/table/sys_user?sysparm_limit=20&sysparm_offset=";
		Integer pagePagination = 0;
		
		pagePagination = page;
		
		url += pagePagination.toString();
		Integer paginationNext = pagePagination + 1;
		
		Integer paginationPrevious = 0;
		if(page>0) {
			paginationPrevious = page - 1;
		}
		
		String urlNext = "users?page=" + paginationNext;
		String urlPrevious = "users?page=" + paginationPrevious;

		try {
			
			HttpHeaders headers = new HttpHeaders();
			headers.set("Authorization", "Bearer " + request.getSession().getAttribute("access_token"));

			HttpEntity<String> entity = new HttpEntity<String>(headers);

			ResponseEntity<RestResponse> responseEntity2 = restTemplate.exchange(url, 
					HttpMethod.GET, entity, RestResponse.class);

			RestResponse restResponse = responseEntity2.getBody();
			Result[] results = restResponse.getResult();
			
			
			model.addAttribute("users", results);
			
			
			
			model.addAttribute("paginationFrom", pagePagination*20+1);
			model.addAttribute("paginationTo", pagePagination*20+20);
			
			model.addAttribute("paginationNext", urlNext);
			model.addAttribute("paginationPrevious", urlPrevious);
			
			
			
			model.addAttribute("pagination", results.length);

		} catch (HttpClientErrorException e) {
			System.out.println(e.getStatusCode());
			if (e.getStatusCode().is4xxClientError()) {
				model.addAttribute("serverMessage", e.getStatusCode());

				return "redirect:/login";
			} else {
				return "redirect:/login";
			}
		} catch (RestClientException e) {

			System.out.println(e.getMessage());
			model.addAttribute("serverMessage", "ServiceNow is in deep sleep");
			return "404";
		}
		catch (NullPointerException e) {
			System.out.println("Error: "+e.getMessage());
		}
		
		return "users";

	}

	@PostMapping("/add-user")
	public String processForm() {

		
		return "login";
	}

}
