package com.companyphone.carmanagement.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;

import com.companyphone.carmanagement.entity.OauthToken;
import com.companyphone.carmanagement.entity.User;
import com.companyphone.carmanagement.utils.AES;
import com.google.gson.Gson;

@Controller
public class AuthController {
	
	@Value("${secretKey.string}")
	private String secretKey;

	String instance = "https://dev90361.service-now.com";
	MultiValueMap<String, String> bodyMap = new LinkedMultiValueMap<>();
	HttpHeaders headers = new HttpHeaders();

	// Constructor
	public AuthController() {

		this.headers.add("Content-Type", "application/x-www-form-urlencoded");

		this.bodyMap.add("grant_type", "password");
		this.bodyMap.add("client_id", "24f8ac0193c90010eeaf2a0b9529778b");
		this.bodyMap.add("client_secret", "A*.wVB-hhs");
	}

	@GetMapping("/login")
	public String doLogin(RestTemplate restTemplate, Model model) {
		model.addAttribute("user", new User());
		
		
		
		
	     
	    String originalString = "howtodoinjava.com";
	    String encryptedString = AES.encrypt(originalString, this.secretKey) ;
	    String decryptedString = AES.decrypt(encryptedString, this.secretKey) ;
	     
	    System.out.println(originalString);
	    System.out.println(encryptedString);
	    System.out.println(decryptedString);
		
		return "login";

	}

	@PostMapping("/auth_login")
	public String processForm(@ModelAttribute 
			User user, RestTemplate restTemplate, 
			HttpServletRequest request) {

		System.out.println(user.getUsername());
		

		this.bodyMap.add("username", user.getUsername());
		this.bodyMap.add("password", user.getPassword());

		String url = instance + "/oauth_token.do";

		HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(bodyMap, headers);

		String result = restTemplate.postForObject(url, requestEntity, String.class);

		Gson gson = new Gson();
		OauthToken json = gson.fromJson(result, OauthToken.class);

		System.out.println(json.getAccess_token());
		
		request.getSession().setAttribute("access_token", json.getAccess_token());
		request.getSession().setAttribute("username", user.getUsername());
		
		return "redirect:/homepage";
	}

}
