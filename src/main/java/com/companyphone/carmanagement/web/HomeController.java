package com.companyphone.carmanagement.web;

import java.lang.reflect.Array;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.companyphone.carmanagement.entity.Car;
import com.companyphone.carmanagement.entity.OauthToken;
import com.companyphone.carmanagement.entity.Order;
import com.companyphone.carmanagement.utils.AES;
import com.google.gson.Gson;
import rest.RestResponse;
import rest.Result;
import rest.ResultUniqueResponse;

@Controller
public class HomeController {

	@Value("${secretKey.string}")
	private String secretKey;

	String instance = "https://dev90361.service-now.com";
	MultiValueMap<String, String> bodyMap = new LinkedMultiValueMap<>();
	HttpHeaders headers = new HttpHeaders();

	// Constructor
	public HomeController() {

	}

	@PostMapping("add-car")
	public String processAddCar(Model model, HttpServletRequest request, @ModelAttribute Car car,
			RestTemplate restTemplate) {

		this.bodyMap = new LinkedMultiValueMap<>();

		this.headers.add("Content-type", "application/json");
		this.headers.add("Accept", "application/json");

		String encryptedValue = AES.encrypt(car.getName(), this.secretKey);

		this.bodyMap.add("name", encryptedValue);

		String url = instance + "/api/now/table/x_76004_carmgmt_car";

		this.headers.set("Authorization", "Bearer " + request.getSession().getAttribute("access_token"));
		HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(this.bodyMap, this.headers);

		String result = restTemplate.postForObject(url, requestEntity, String.class);

		Gson gson = new Gson();
		// OauthToken json = gson.fromJson(result, OauthToken.class);

		// model.addAttribute("serverMessage", "Car created");

		return "add-car";
	}

	@GetMapping("/add-car")
	public String addCarPage(Model model) {

		model.addAttribute("car", new Car());
		return "add-car";
	}

	@GetMapping("/car/{id}")
	public String editCarPage(Model model, @PathVariable("id") String id, HttpServletRequest request,
			RestTemplate restTemplate) {

		String url = instance + "/api/now/table/x_76004_carmgmt_car/" + id;
		Car car = new Car();

		try {
			HttpHeaders headers = new HttpHeaders();
			headers.set("Authorization", "Bearer " + request.getSession().getAttribute("access_token"));

			HttpEntity<String> entity = new HttpEntity<String>(headers);

			ResponseEntity<ResultUniqueResponse> responseEntity2 = restTemplate.exchange(url, HttpMethod.GET, entity,
					ResultUniqueResponse.class);

			ResultUniqueResponse restResponse = responseEntity2.getBody();

			Result result = restResponse.getResult();

			String carName = result.getName();
			String subString = carName.substring(0, carName.length() - 1);
			String toDecrypt = subString.substring(1);
			String decryptedString = AES.decrypt(toDecrypt, this.secretKey);

			car.setName(decryptedString);

		} catch (HttpClientErrorException e) {
			System.out.println(e.getStatusCode());
			if (e.getStatusCode().is4xxClientError()) {
				model.addAttribute("serverMessage", e.getStatusCode());

				return "redirect:/login";
			} else {
				return "redirect:/login";
			}
		} catch (RestClientException e) {

			System.out.println(e.getMessage());
			model.addAttribute("serverMessage", "ServiceNow is in deep sleep");
			return "404";
		}

		model.addAttribute("car", car);

		return "edit-car";
	}

	@GetMapping("/cars")
	public String getCarsPage(RestTemplate restTemplate, Model model, HttpServletRequest request) {

		String url = instance + "/api/now/table/x_76004_carmgmt_car";

		try {
			HttpHeaders headers = new HttpHeaders();
			headers.set("Authorization", "Bearer " + request.getSession().getAttribute("access_token"));

			HttpEntity<String> entity = new HttpEntity<String>(headers);

			ResponseEntity<RestResponse> responseEntity2 = restTemplate.exchange(url, HttpMethod.GET, entity,
					RestResponse.class);

			RestResponse restResponse = responseEntity2.getBody();

			Result[] results = restResponse.getResult();

			model.addAttribute("cars", results);

			for (Result result : results) {
				String resultName = result.getName();

				String subString = resultName.substring(0, resultName.length() - 1);
				String toDecrypt = subString.substring(1);
				String decryptedString = AES.decrypt(toDecrypt, this.secretKey);

				result.setName(decryptedString);

			}

		} catch (HttpClientErrorException e) {
			System.out.println(e.getStatusCode());
			if (e.getStatusCode().is4xxClientError()) {
				model.addAttribute("serverMessage", e.getStatusCode());

				return "redirect:/login";
			} else {
				return "redirect:/login";
			}
		} catch (RestClientException e) {

			System.out.println(e.getMessage());
			model.addAttribute("serverMessage", "ServiceNow is in deep sleep");
			return "404";
		}
		return "car";
	}

	private SecretKey secretKey() {
		SecretKey secretKey;
		try {
			KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
			keyGenerator.init(128);
			secretKey = keyGenerator.generateKey();
		} catch (Exception e) {
			return null;
		}

		return secretKey;
	}

	@GetMapping("/homepage")
	public String getHomePage(HttpServletRequest request, 
		RestTemplate restTemplate, 
		Model model) {
	
	String url = instance + "/api/now/table/sys_user?sysparm_query=active%3Dtrue";

	try {
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "Bearer " + request.getSession().getAttribute("access_token"));

		HttpEntity<String> entity = new HttpEntity<String>(headers);

		ResponseEntity<RestResponse> responseEntity2 = restTemplate.exchange(url, 
				HttpMethod.GET, entity, RestResponse.class);

		RestResponse restResponse = responseEntity2.getBody();
		Result[] results = restResponse.getResult();
		
		System.out.println("results.length"+results.length);
		model.addAttribute("resultslength", results.length);
		

	} catch (HttpClientErrorException e) {
		System.out.println(e.getStatusCode());
		if (e.getStatusCode().is4xxClientError()) {
			model.addAttribute("serverMessage", e.getStatusCode());

			return "redirect:/login";
		} else {
			return "redirect:/login";
		}
	} catch (RestClientException e) {

		System.out.println(e.getMessage());
		model.addAttribute("serverMessage", "ServiceNow is in deep sleep");
		return "404";
	}
	catch (NullPointerException e) {
		System.out.println("Error: "+e.getMessage());
	}


		return "homepage";
	}
	

	@GetMapping("/new-order")
	public String getNewOrderPage(RestTemplate restTemplate, Model model, HttpServletRequest request) {

		String url = instance + "/api/now/table/x_76004_carmgmt_car";

		try {
			HttpHeaders headers = new HttpHeaders();
			headers.set("Authorization", "Bearer " + request.getSession().getAttribute("access_token"));

			HttpEntity<String> entity = new HttpEntity<String>(headers);

			ResponseEntity<RestResponse> responseEntity2 = restTemplate.exchange(url, HttpMethod.GET, entity,
					RestResponse.class);

			RestResponse restResponse = responseEntity2.getBody();

			Result[] results = restResponse.getResult();

			model.addAttribute("cars", results);

			Order order = new Order();
			order.setUser("" + request.getSession().getAttribute("username"));

			model.addAttribute("order", order);

			for (Result result : results) {
				String resultName = result.getName();

				String subString = resultName.substring(0, resultName.length() - 1);
				String toDecrypt = subString.substring(1);
				String decryptedString = AES.decrypt(toDecrypt, this.secretKey);

				result.setName(decryptedString);

			}

		} catch (HttpClientErrorException e) {
			System.out.println(e.getStatusCode());
			if (e.getStatusCode().is4xxClientError()) {
				model.addAttribute("serverMessage", e.getStatusCode());

				return "redirect:/login";
			} else {
				return "redirect:/login";
			}
		} catch (RestClientException e) {

			System.out.println(e.getMessage());
			model.addAttribute("serverMessage", "ServiceNow is in deep sleep");
			return "404";
		}

		return "new-order";
	}

	@PostMapping("new-order")
	public String processNewOrder(Model model, HttpServletRequest request, @ModelAttribute Order order,
			RestTemplate restTemplate) {

		this.bodyMap = new LinkedMultiValueMap<>();

		this.headers.add("Content-type", "application/json");
		this.headers.add("Accept", "application/json");

		System.out.println(order.getCar());
		System.out.println(order.getUser());

		this.bodyMap.add("car", "" + order.getCar());
		this.bodyMap.add("user", "" + order.getUser());

		String url = instance + "/api/x_76004_carmgmt/create_order";

		this.headers.set("Authorization", "Bearer " + request.getSession().getAttribute("access_token"));
		HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(this.bodyMap, this.headers);

		String result = restTemplate.postForObject(url, requestEntity, String.class);

		Gson gson = new Gson();

		return "new-order";
	}
	
	@GetMapping("/my-order")
	public String getMyOrderPage(RestTemplate restTemplate, Model model, HttpServletRequest request) {

		String url = instance + "/api/now/table/x_76004_carmgmt_car";

		try {
			HttpHeaders headers = new HttpHeaders();
			headers.set("Authorization", "Bearer " + request.getSession().getAttribute("access_token"));

			HttpEntity<String> entity = new HttpEntity<String>(headers);

			ResponseEntity<RestResponse> responseEntity2 = restTemplate.exchange(url, HttpMethod.GET, entity,
					RestResponse.class);

			RestResponse restResponse = responseEntity2.getBody();

			Result[] results = restResponse.getResult();

			model.addAttribute("cars", results);

			for (Result result : results) {
				String resultName = result.getName();

				String subString = resultName.substring(0, resultName.length() - 1);
				String toDecrypt = subString.substring(1);
				String decryptedString = AES.decrypt(toDecrypt, this.secretKey);

				result.setName(decryptedString);

			}

		} catch (HttpClientErrorException e) {
			System.out.println(e.getStatusCode());
			if (e.getStatusCode().is4xxClientError()) {
				model.addAttribute("serverMessage", e.getStatusCode());

				return "redirect:/login";
			} else {
				return "redirect:/login";
			}
		} catch (RestClientException e) {

			System.out.println(e.getMessage());
			model.addAttribute("serverMessage", "ServiceNow is in deep sleep");
			return "404";
		}
		return "myorder";
	}
	
	

}
