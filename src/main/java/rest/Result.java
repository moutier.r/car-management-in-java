package rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true) 
public class Result { 
	
	private String number; 
	private String sys_id; 
	private String sys_updated_by; 
	private String sys_created_on; 
	private String name; 
	private String sys_mod_count; 
	private String sys_updated_on; 
	private String sys_tags; 
	private String sys_created_by; 
	
	//User
	private String user_name;
	
	
	
	
	
	
	public String getUser_name() {
		return user_name;
	}


	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getSys_created_on() {
		return sys_created_on;
	}


	public void setSys_created_on(String sys_created_on) {
		this.sys_created_on = sys_created_on;
	}


	public String getSys_mod_count() {
		return sys_mod_count;
	}


	public void setSys_mod_count(String sys_mod_count) {
		this.sys_mod_count = sys_mod_count;
	}


	public String getSys_updated_on() {
		return sys_updated_on;
	}


	public void setSys_updated_on(String sys_updated_on) {
		this.sys_updated_on = sys_updated_on;
	}


	public String getSys_tags() {
		return sys_tags;
	}


	public void setSys_tags(String sys_tags) {
		this.sys_tags = sys_tags;
	}


	public String getSys_created_by() {
		return sys_created_by;
	}


	public void setSys_created_by(String sys_created_by) {
		this.sys_created_by = sys_created_by;
	}


	public String getNumber() {
		return number;
	}


	public void setNumber(String number) {
		this.number = number;
	}


	public String getSys_id() {
		return sys_id;
	}


	public void setSys_id(String sys_id) {
		this.sys_id = sys_id;
	}


	public String getSys_updated_by() {
		return sys_updated_by;
	}


	public void setSys_updated_by(String sys_updated_by) {
		this.sys_updated_by = sys_updated_by;
	}


	
	
	public Result() {
		
	} 
	
	
	@Override 
	public String toString() { 
		
		return "Result [number=" + this.number + ", sys_id=" + this.sys_id + ", sys_updated_by=" + this.sys_updated_by + "]";
		
	}
	
}


